import React, { Component } from 'react';

const axios = require('axios');

class App extends Component {
	state = {
		persons: [],
		name: '',
		email: '',
		phone: '',
	};

	componentDidMount() {
		axios
			.get(`http://localhost:3001/score`, {
				headers: {
					'Content-Type': 'application/json',
					'Access-Control-Allow-Origin': origin,
					'Access-Control-Allow-Credentials': 'true',
				},
			})
			.then(res => {
				console.log(res.data);
				const persons = res.data.studentScore;
				this.setState({ persons });
			});
	}

	handleChangeName = event => {
		this.setState({ name: event.target.value });
	};

	handleChangeEmail = event => {
		this.setState({ email: event.target.value });
	};

	handleChangePhone = event => {
		this.setState({ phone: event.target.value });
	};

	handleSubmit = event => {
		event.preventDefault();

		const data = {
			name: this.state.name,
			email: this.state.email,
			phone: this.state.phone,
			dob:null
		};
		console.log(data);
		
		axios.post(`http://localhost:3001/register`, { data }).then(res => {
			const persons = res.data.studentScore;
			persons.push(data)
		});
	};

	render() {
		return (
			<div className="container">
				<form onSubmit={this.handleSubmit}>
					<label id="labe">
						Person Name:&emsp;
						<input type="text" name="name" onChange={this.handleChangeName} />
					</label>
					&emsp;
					<label id="labe">
						Email:&emsp;
						<input type="text" name="email" onChange={this.handleChangeEmail} />
					</label>
					&emsp;
					<label id="labe">
						Phone:&emsp;
						<input type="text" name="phone" onChange={this.handleChangePhone} />
					</label>
					&emsp;
					<button type="submit" className="btn btn-primary btn-sm">
						Add
					</button>
				</form>

				<table className="table table-striped">
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Phone</th>
						<th>Course</th>
						<th>Score</th>
						<th>Average</th>
					</tr>
					{this.state.persons.map(person => (
						<tr>
							<td>{person.id}</td>
							<td>{person.name}</td>
							<td>{person.phone}</td>
							<td>{person.course_name}</td>
							<td>{person.score}</td>
							<td>{person.average}</td>
						</tr>
					))}
				</table>
			</div>
		);
	}
}

export default App;
